package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import moocs.ejb.entity.Committee;
@Remote
public interface ICommitteeManagementRemote {
	void VoteCommittee(Committee c,boolean state);
	List<Committee> findAllCommittee0();
	public void AddCommittee(Committee c);
}
