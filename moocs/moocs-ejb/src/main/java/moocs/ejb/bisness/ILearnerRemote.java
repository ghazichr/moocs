package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Learner;
@Remote
public interface ILearnerRemote {
	 void  addLearner(Learner l);
	 Learner  affLearner(Learner l);
	List<Learner> listAllLearner ();
	 void updateLearner(Learner l);
	 void removeLearner(Learner l);
	void DesapproveTrainer(Learner l);
	void approveTrainer(Learner l);
	List<Learner> findAllLeanerDemande1();
	 void LearnerDemande(Learner l);
	 List<Learner> findLeanerEnabletrue();
	 public byte[] loadImage(int id);
	
	
	
}
