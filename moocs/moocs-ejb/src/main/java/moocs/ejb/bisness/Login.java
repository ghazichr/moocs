package moocs.ejb.bisness;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.User;

@Stateless
public class Login implements ILogin{
	@PersistenceContext
    EntityManager em;
	
	public User findLoginPassword(String email,String password){
		User u=new User();
	 	u=(User)em.createQuery("select u from User as u where u.email=?1 and u.password=?2")
		    .setParameter("1",email)
		    .setParameter("2",password)
		    .getSingleResult();
	 	if(u==null){
	 		return null;
	 	}
	 	return u;
	}
	
}
