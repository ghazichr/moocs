package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Answer;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;
@Local
public interface IQuestionLocal {
	void addQuestion(Question a);
	Question affQuestion(Question a);
	Question affQuestionName(Question a);
	void update(Question a);
	void remove(Question a);
	List<Question> afficherQcmQuestion(Qcm q);
	void affectQuestionAnswer(Question question, List<Answer> lAnswers) ;
}
