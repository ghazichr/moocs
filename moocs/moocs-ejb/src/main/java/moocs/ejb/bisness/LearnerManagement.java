package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import moocs.ejb.entity.Learner;
@Stateless
public class LearnerManagement implements ILearnerRemote ,ILearnerLocal {
	
	@PersistenceContext
	EntityManager em ;

	
	public void addLearner(Learner l) {
		em.persist(l);
		em.flush();
		
	}
	
	public void approveTrainer(Learner l) {
		l.setEnable(true);
		l.setDemande(false);
		em.merge(l);

	}
	public void DesapproveTrainer(Learner l) {
		l.setDemande(false);
		em.merge(l);
	}
	
	   public byte[] loadImage(int id){
	       // return em.find(Learner.class, id).getPicture();
		   return null ;
	    }
	
	public List<Learner> findAllLeanerDemande1() {
		return em.createQuery("select l from Learner as l where l.demande= ?1").setParameter("1",true).getResultList();
	}
	

	public List<Learner> findLeanerEnabletrue() {
		return em.createQuery("select l from Learner as l where l.enable= ?1").setParameter("1",true).getResultList();
	}
	public void LearnerDemande(Learner l) {
		l.setDemande(true);
		em.merge(l);
	}

	
	public Learner affLearner(Learner l) {
		
		return em.find(Learner.class,l.getIdUser()) ;
	}

	
	
	


	@Override
	public List<Learner> listAllLearner() {
		
		return em.createQuery("select l from Learner as l").getResultList();
	}

	@Transactional
	@Override
	public void updateLearner(Learner l) {
		em.merge(l);
		
	}


	@Override
	public void removeLearner(Learner l) {
		em.remove(em.merge(l));
		
	}

}
