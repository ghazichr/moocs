package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Answer;
import moocs.ejb.entity.Question;
@Local
public interface IAnswerLocal {
	void addAnswer(Answer a);
	Answer affAnswer(Answer a);
	void update(Answer a);
	void remove(Answer a);
	List<Answer> afficherQuestionAnswers(Question q);
}
