package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Certificate;
import moocs.ejb.entity.Examen;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;
@Local
public interface IExamenLocal {
	void addExamen(Examen q);

	long addExamenLong(Examen q);

	Examen affExamen(Examen q);
	
	Qcm affQcm(Examen q);

	Examen affExamenName(Examen q);

	Examen affExamenId(int q);

	void update(Examen q);

	void remove(Examen q);

	List<Examen> afficherAllExamen();

	void affectExamenQuestion(Examen examen, List<Question> lQuestion);

	void affectExamenCertificat(Examen examen, Certificate certificate);
}
