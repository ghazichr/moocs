package moocs.ejb.bisness;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Trainer;

@Stateless
public class TrainerComitee implements ITrainerComiteeLocal{
	@PersistenceContext
	EntityManager em;
	public void TrainerComitee(Trainer t , boolean demande, String CvUrl ){
		t.setCvUrl(CvUrl);
		t.setDemande(true);
		em.merge(t);
		
		
	}

}
