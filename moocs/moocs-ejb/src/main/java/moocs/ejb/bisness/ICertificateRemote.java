package moocs.ejb.bisness;

import java.sql.Blob;
import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Certificate;

@Remote
public interface ICertificateRemote {
	void addCertificate(Certificate q);

	long addCertificateLong(Certificate q);

	Certificate affCertificate(Certificate q);

	Certificate affCertificateName(Certificate q);

	Certificate affCertificateId(int q);
	
	void certificateAddImage(Certificate q,Blob image);

	void update(Certificate q);

	void remove(Certificate q);
	
	public List<Certificate> affAllCertifs() ;
}

