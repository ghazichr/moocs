package moocs.ejb.bisness;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Badge;
import moocs.ejb.entity.BadgeLearne;
import moocs.ejb.entity.BadgeLearnerPK;
import moocs.ejb.entity.Learner;

@Stateless
public class BadgeLearnerManagement implements IBadgeLearnerLocal {
	@PersistenceContext
	EntityManager em;
	@Override
	public void addBadgeLearner(Badge b, Learner l,int note) {
		BadgeLearne BL=new BadgeLearne();
		BadgeLearnerPK PK = new BadgeLearnerPK();
		PK.setIdBadge(b.getIdBadge());
		PK.setIdUser(l.getIdUser());
		BL.setPk(PK);
		BL.setNote(note);
		em.persist(BL);
	}

}
