package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Course;
@Remote
public interface ICourseSchedulingRemote {
	public void addCourse(Course c);
	public long addCourseLong(Course c);
	public Course affCourse(Course q);
	public List<Course> afficherAllCourse();
	public void update(Course e);
	public void remove(Course e);
	public List<Course> findAll();
	public Course findById(int id);
}
