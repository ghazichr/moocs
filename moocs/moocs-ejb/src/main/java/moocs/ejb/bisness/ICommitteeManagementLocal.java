package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Committee;
@Local
public interface ICommitteeManagementLocal {
	void VoteCommittee(Committee c,boolean state);
	List<Committee> findAllCommittee0();
	public void AddCommittee(Committee c);
}
