package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Answer;
import moocs.ejb.entity.Question;

@Stateless
public class AnswerManagement implements IAnswerRemote,IAnswerLocal {
	@PersistenceContext
	EntityManager em;

	@Override
	public void addAnswer(Answer a) {
		em.persist(a);
		
	}

	public Answer affAnswer(Answer a) {
		return em.find(Answer.class,a.getIdAnswer());
	}

	@Override
	public List<Answer> afficherQuestionAnswers(Question q) {
		return em.createQuery("select a from Answer as a where a.question= ?1")
  				.setParameter("1",q)
  				.getResultList();
	}

	@Override
	public void update(Answer a) {
		em.merge(a);
		
	}

	@Override
	public void remove(Answer a) {
		a=em.find(Answer.class, a.getIdAnswer());
		a.setQuestion(null);
		em.remove(em.merge(a));
		
	}

}
