package moocs.ejb.bisness;

import java.sql.Blob;
import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Badge;

@Remote
public interface IbadgeRemote {
	void addBadge(Badge q);

	long addBadgeLong(Badge q);

	Badge affBadge(Badge q);

	Badge affBadgeName(Badge q);

	Badge affBadgeId(int q);

	void badgeAddImage(Badge b ,Blob image);

	void update(Badge q);

	void remove(Badge q);
	
	public List<Badge> affAllBadges();

}
