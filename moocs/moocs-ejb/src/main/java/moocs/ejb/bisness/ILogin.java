package moocs.ejb.bisness;

import javax.ejb.Remote;

import moocs.ejb.entity.User;

@Remote
public interface ILogin {
	User findLoginPassword(String email,String password);
}
