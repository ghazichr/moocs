package moocs.ejb.bisness;

import java.sql.Blob;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Certificate;
@Stateless
public class CertificateManagement implements ICertificateRemote,ICertificateLocal {
	@PersistenceContext
	EntityManager em;
	@Override
	public void addCertificate(Certificate q) {
		em.persist(q);
		
	}

	@Override
	public long addCertificateLong(Certificate q) {
		em.persist(q);
		return q.getIdCertificate();
	}

	@Override
	public Certificate affCertificate(Certificate q) {
		return em.find(Certificate.class, q.getIdCertificate());
	}
	
	public List<Certificate> affAllCertifs() {
		return em.createQuery("select c from Certificate as c")
  				.getResultList();
	}


	@Override
	public Certificate affCertificateName(Certificate q) {
		return (Certificate) em.createQuery("select q from Certificate as q where q.nameCertificate= ?1")
				.setParameter("1", q.getNameCertificate()).getSingleResult();	
	}

	@Override
	public Certificate affCertificateId(int q) {
		return em.find(Certificate.class, q);
	}

	@Override
	public void certificateAddImage(Certificate q,Blob image) {
		q.setImage(image);
		em.merge(q);
		
	}

	@Override
	public void update(Certificate q) {
		em.merge(q);
		
	}

	@Override
	public void remove(Certificate q) {
		em.remove(em.merge(q));
		
	}

}
