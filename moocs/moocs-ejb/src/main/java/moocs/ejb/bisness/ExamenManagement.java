package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Certificate;
import moocs.ejb.entity.Examen;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;
@Stateless
public class ExamenManagement implements IExamenRemote, IExamenLocal {
	@PersistenceContext
	EntityManager em;
	@Override
	public void addExamen(Examen q) {
		em.persist(q);
		
	}

	@Override
	public long addExamenLong(Examen q) {
		em.persist(q);
		return q.getIdQcm();
	}

	@Override
	public Examen affExamen(Examen q) {
		return em.find(Examen.class, q.getIdQcm());
	}
	public Qcm affQcm(Examen q) {
		Examen ex = em.find(Examen.class, q.getIdQcm());
		Qcm qc = new Qcm(ex.getIdQcm(),ex.getName());
		return qc;
	}
	@Override
	public Examen affExamenName(Examen q) {
		return (Examen) em.createQuery("select q from Examen as q where q.name= ?1")
				.setParameter("1", q.getName()).getSingleResult();	
	}

	@Override
	public Examen affExamenId(int q) {
		return em.find(Examen.class, q);
	}

	@Override
	public void update(Examen q) {
		em.merge(q);
		
	}

	@Override
	public void remove(Examen q) {
		em.remove(em.merge(q));
		
	}

	@Override
	public List<Examen> afficherAllExamen() {
		return em.createQuery("select q from Examen as q")
  				.getResultList();
	}

	@Override
	public void affectExamenQuestion(Examen examen, List<Question> lQuestion) {
		examen.Affect(lQuestion);
		em.merge(examen);
		
	}

	@Override
	public void affectExamenCertificat(Examen examen, Certificate certificate) {
		examen.setCertificate(certificate);
		em.merge(examen);
		
	}

}
