package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;

@Stateless
public class QcmManager implements IQcmRemote,IQcmLocal {
	@PersistenceContext
	EntityManager em;
	
	@Override
	public void addQcm(Qcm q) {
		em.persist(q);
		
	}
	@Override
	public long addQcmLong(Qcm q) {
		em.persist(q);
		return q.getIdQcm();
		
	}

	@Override
	public Qcm affQcm(Qcm q) {
		return em.find(Qcm.class,q.getIdQcm());
	}
	
	@Override
	public Qcm affQcmId(int q) {
		return em.find(Qcm.class,q);
	}
	@Override
	public List<Qcm> afficherAllQcm() {
		return em.createQuery("select q from Qcm as q")
  				.getResultList();
		}
	@Override
	public void update(Qcm q) {

		em.merge(q);
	}
	@Override
	public void remove(Qcm q) {
		
		//q=em.find(Qcm.class, q.getIdQcm());
		em.remove(em.merge(q));
	}
	@Override
	public void affectQcmQuestion(Qcm qcm, List<Question> lQuestion) {
		qcm.Affect(lQuestion);
		em.merge(qcm);
		
	}
	@Override
	public Qcm affQcmName(Qcm q) {
		
		return (Qcm) em.createQuery("select q from Qcm as q where q.name= ?1")
				.setParameter("1", q.getName()).getSingleResult();
	}
}
