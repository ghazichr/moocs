package moocs.ejb.bisness;

import java.sql.Blob;
import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Badge;
@Local
public interface IbadgeLocal {
	void addBadge(Badge q);

	long addBadgeLong(Badge q);

	Badge affBadge(Badge q);

	Badge affBadgeName(Badge q);

	Badge affBadgeId(int q);

	void badgeAddImage(Badge b ,Blob image);

	void update(Badge q);

	void remove(Badge q);
	
	public List<Badge> affAllBadges();
	

}
