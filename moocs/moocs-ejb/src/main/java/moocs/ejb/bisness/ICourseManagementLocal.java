package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Course;
@Local
public interface ICourseManagementLocal {
	void validateCourse(Course c, boolean enable);
	List<Course> FindAllCourse0();
	void addCourse(Course c);
	public Course findCourse(Course c);
	public List<Course> FindAllPopularedCourse();
	List<Course> findallCourseByUser(int idT);
}
