package moocs.ejb.bisness;

import javax.ejb.Local;

import moocs.ejb.entity.Trainer;

@Local
public interface ITrainerComiteeLocal {
	public void TrainerComitee(Trainer t , boolean demande, String CvUrl );

}
