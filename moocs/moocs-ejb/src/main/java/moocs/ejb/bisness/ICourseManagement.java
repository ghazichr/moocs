package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Course;
@Remote
public interface ICourseManagement {
	void validateCourse(Course c, boolean enable);
	List<Course> FindAllCourse0();
	void addCourse(Course c);
	public Course findCourse(Course c);
	public List<Course> FindAllPopularedCourse();
}
