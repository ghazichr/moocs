package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Badge;
import moocs.ejb.entity.Examen;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;
import moocs.ejb.entity.Quizz;
@Stateless
public class QuizzManagement implements IQuizzRemote ,IQuizzLocal {
	@PersistenceContext
	EntityManager em;
	@Override
	public void addQuizz(Quizz q) {
		em.persist(q);
		
	}

	@Override
	public long addQuizzLong(Quizz q) {
		em.persist(q);
		return q.getIdQcm();
	}

	@Override
	public Quizz affQuizz(Quizz q) {
		return em.find(Quizz.class, q.getIdQcm());
	}
	public Qcm affQcm(Quizz q) {
		Quizz ex = em.find(Quizz.class, q.getIdQcm());
		Qcm qc = new Qcm(ex.getIdQcm(),ex.getName());
		return qc;
	}
	@Override
	public Quizz affQuizzName(Quizz q) {
		return (Quizz) em.createQuery("select q from Quizz as q where q.name= ?1")
				.setParameter("1", q.getName()).getSingleResult();	}

	@Override
	public Quizz affQuizzId(int q) {

		return em.find(Quizz.class,q);
	}

	@Override
	public void update(Quizz q) {
		em.merge(q);
		
	}

	@Override
	public void remove(Quizz q) {
		em.remove(em.merge(q));
		
	}

	@Override
	public List<Quizz> afficherAllQuizz() {
		return em.createQuery("select q from Quizz as q")
  				.getResultList();
	}

	@Override
	public void affectQuizzQuestion(Quizz quizz, List<Question> lQuestion) {
		quizz.Affect(lQuestion);
		em.merge(quizz);
		
	}

	@Override
	public void affectQuizzCertificat(Quizz quizz, Badge badge) {
		quizz.setBadge(badge);
		em.merge(quizz);
		
	}

}
