package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Trainer;
import moocs.ejb.entity.User;

@Stateless
public class TrainerManagement implements ITrainerRemote , ITrainerLocal{
	@PersistenceContext
	EntityManager em;
	
	public void  addTrainer(Trainer t) {
		em.persist(t);
		
	}

	public Trainer  affTrainer(Trainer t) {
		
		return em.find(Trainer.class,t.getIdUser());
		
	}
	
	
	
	
	public Trainer  affTrainer(int id){
		return  em.find(Trainer.class,id);
	}
	//version2
	public void validateTrainer(Trainer t, boolean status) {
		t = em.find(Trainer.class, t.getIdUser());
		int number = t.getVoteNumbr();
		if (number < 3) {
			number++;
			t.setVoteNumbr(number);
			em.merge(t);
			number--;
		}
		if (number == 3) {
			t.setDemande(false);
			t.setEnable(true);
			em.merge(t);
		}
		if (status == false) {
			t.setDemande(false);
			em.merge(t);
		}
	}
    

    
    public List<Trainer> listalltrainer() {
  		return em.createQuery("select t from Trainer as t").getResultList();	
  	}
    
    
    public List<Trainer> findAllTrainerDemande1() {
		return em.createQuery("select t from Trainer as t where t.demande= ?1").setParameter("1", true).getResultList();
	}
    
    public List<Trainer> findAllTrainerName(String name) {
  		return em.createQuery("select t from Trainer as t where t.firstName= ?1").setParameter("1", name).getResultList();
  	}
    
    public List<Trainer> findPopularTrainer(){
    	
    	return em.createQuery("select t from Trainer as t where t.note> ?1").setParameter("1", 3).getResultList();
    }
    

    public List<Trainer> findTrainerEnabletrue() {
		return em.createQuery("select t from Trainer as t where t.enable= ?1").setParameter("1", true).getResultList();
	}
    
	// version 1 peut etre supprimer avec l'interface !!
	public List<Trainer> findAllTrainer0() {
		return em.createQuery("select t from Trainer as t where t.enable= ?1").setParameter("1", false).getResultList();
	}
	@Override
	public void updateTrainer(Trainer t) {
		em.merge(t);
		
	}

	@Override
	public void removeTrainer(Trainer t) {
		em.remove(em.merge(t));
		
	}

	public void TrainerDemande(Trainer t) {
		t.setDemande(true);
		em.merge(t);

	}



	}
