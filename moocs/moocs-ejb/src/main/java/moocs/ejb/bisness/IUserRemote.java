package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Learner;
import moocs.ejb.entity.User;
@Remote
public interface IUserRemote {
	public void  addUser(User U);
	public User  affUser(User U);
	public User  affUser(int id);
	List<User> ListAllUser ();
	public void updateUser(User U) ;
	public void removeUser(User U) ;
	public User authenticate(String email, String password);

}
