package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;

@Remote
public interface IQcmRemote {
	void addQcm(Qcm q);

	long addQcmLong(Qcm q);

	Qcm affQcm(Qcm q);
	Qcm affQcmName(Qcm q);
	Qcm affQcmId(int q);
	void update(Qcm q);
	void remove(Qcm q);

	List<Qcm> afficherAllQcm();
	void affectQcmQuestion(Qcm qcm, List<Question> lQuestion) ;
}
