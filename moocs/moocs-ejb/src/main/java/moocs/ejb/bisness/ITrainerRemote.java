package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Trainer;


@Remote
public interface ITrainerRemote {
	
	void addTrainer(Trainer t);

	Trainer affTrainer(Trainer t);

	Trainer affTrainer(int id);

	void validateTrainer(Trainer t, boolean status);

	List<Trainer> listalltrainer();

	void updateTrainer(Trainer t);

	void removeTrainer(Trainer t);

	List<Trainer> findAllTrainerDemande1();

	void TrainerDemande(Trainer t);

	List<Trainer> findAllTrainer0();

	List<Trainer> findTrainerEnabletrue();
	
	List<Trainer> findAllTrainerName(String name);
	
	List<Trainer> findPopularTrainer();

}
