package moocs.ejb.bisness;

import java.sql.Blob;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Badge;
@Stateless
public class BadgeManagement implements IbadgeRemote , IbadgeLocal {
	@PersistenceContext
	EntityManager em;

	
	public void addBadge(Badge q) {
		em.persist(q);

	}

	@Override
	public long addBadgeLong(Badge q) {
		em.persist(q);
		return q.getIdBadge();
	}

	@Override
	public Badge affBadge(Badge q) {
		return em.find(Badge.class, q.getIdBadge());
	}

	@Override
	public Badge affBadgeName(Badge q) {
		return (Badge) em.createQuery("select q from Badge as q where q.nameBadge= ?1")
				.setParameter("1", q.getNameBadge()).getSingleResult();
	}
	
	public List<Badge> affAllBadges() {
		return em.createQuery("select b from Badge as b")
  				.getResultList();
	}

	@Override
	public Badge affBadgeId(int q) {
		return em.find(Badge.class, q);
	}

	@Override
	public void badgeAddImage(Badge b, Blob image) {
		b.setImage(image);
		em.merge(b);

	}

	@Override
	public void update(Badge q) {
		em.merge(q);

	}

	@Override
	public void remove(Badge q) {
		em.remove(em.merge(q));

	}

}
