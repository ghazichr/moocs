package moocs.ejb.bisness;

import javax.ejb.Local;

import moocs.ejb.entity.Badge;
import moocs.ejb.entity.Learner;

@Local
public interface IBadgeLearnerLocal {
	public void addBadgeLearner(Badge b,Learner l,int note);
}
