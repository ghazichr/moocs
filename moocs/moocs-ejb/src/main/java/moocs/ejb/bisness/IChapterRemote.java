package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Chapter;
import moocs.ejb.entity.Course;
@Remote
public interface IChapterRemote {
	public List<Chapter> afficherCourseChapter(Course q);
	public Chapter affAnswer(Chapter a);
	public void addChapter(Chapter a);
	public void update(Chapter e);
	public void remove(Chapter e);
	public List<Chapter> findAll();
	public Chapter findById(int id);
}
