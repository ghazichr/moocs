package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Answer;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;

@Stateless
public class QuestionManagement implements IQuestionRemote , IQuestionLocal {

	@PersistenceContext
	EntityManager em;

	@Override
	public void addQuestion(Question a) {
		System.out.println("in addQ");
		em.persist(a);
	}

	@Override
	public Question affQuestion(Question a) {

		return em.find(Question.class, a.getIdQuestion());
	}

	public Question affQuestionName(Question a) {

		// return em.find(Question.class,a.getQuestion());
		return (Question) em.createQuery("select q from Question as q where q.question= ?1")
				.setParameter("1", a.getQuestion()).getSingleResult();
	}

	@Override
	public void update(Question a) {
		em.merge(a);

	}

	@Override
	public void remove(Question a) {
		a=em.find(Question.class, a.getIdQuestion());
		a.setQcm(null);
		em.merge(a);
		em.remove(a);
		//em.remove(em.merge(a));

	}

	@Override
	public List<Question> afficherQcmQuestion(Qcm q) {
		System.out.println("in aff");
		return em.createQuery("select a from Question as a where a.qcm= ?1").setParameter("1", q).getResultList();
	}

	@Override
	public void affectQuestionAnswer(Question question, List<Answer> lAnswers) {
		question.Affect(lAnswers);
		em.merge(question);
		
	}

}
