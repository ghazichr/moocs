package moocs.ejb.bisness;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Chapter;
import moocs.ejb.entity.Course;

@Stateless
public class ChapterManagement implements IChapterRemote,IChapterLocal {
	@PersistenceContext
	EntityManager em;

	
	public void addChapter(Chapter a) {
		em.persist(a);
		
	}

	public Chapter affAnswer(Chapter a) {
		return em.find(Chapter.class,a.getIdChapter());
	}
	
	public List<Chapter> afficherCourseChapter(Course q) {
		return   em.createQuery("select a from Chapter as a where a.course.id=?1")
				.setParameter(1, q.getIdCourse())
  				.getResultList();
	}
	
	public void update(Chapter e) {
		em.flush();
		em.merge(e);
		
	}

	public void remove(Chapter e) {
		
		em.remove(em.merge(e));//pour changer l'etat de 
		//detached vers managed
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Chapter> findAll() {
		List<Chapter>chapters=new ArrayList<>();
		chapters=(List<Chapter>) em.createQuery("select e from Chapter e")
				.getResultList();
		return chapters;
	}
	
	@Override
	public Chapter findById(int id) {
		Chapter e =em.find(Chapter.class,id);
		
	    return  e;
	}

}
