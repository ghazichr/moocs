package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Badge;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;
import moocs.ejb.entity.Quizz;

@Remote
public interface IQuizzRemote {
	void addQuizz(Quizz q);

	long addQuizzLong(Quizz q);

	Quizz affQuizz(Quizz q);

	Quizz affQuizzName(Quizz q);
	Qcm affQcm(Quizz q);

	Quizz affQuizzId(int q);

	void update(Quizz q);

	void remove(Quizz q);

	List<Quizz> afficherAllQuizz();

	void affectQuizzQuestion(Quizz quizz, List<Question> lQuestion);

	void affectQuizzCertificat(Quizz quizz, Badge badge);
}
