package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Committee;

@Stateless
public class CommitteeManagement implements ICommitteeManagement,ICommitteeManagementLocal {
	@PersistenceContext
	EntityManager em;

	// version 1
	public void VoteCommittee(Committee c, boolean state) {
		c = em.find(Committee.class, c.getIdUser());
		if (c.getVoteNumbr() < 3 && state == true) {
			int nbr = c.getVoteNumbr();
			c.setVoteNumbr(nbr++);
			em.merge(c);
			c.setVoteNumbr(nbr--);
		}
		if (c.getVoteNumbr() == 3 && state == true) {
			c.setEnable(true);
			em.merge(c);
		}
		if (state == false)
			em.remove(c);
	}

	// version 1
	public List<Committee> findAllCommittee0() {
		return em.createQuery("select c from Committee as c where c.enable= ?1").setParameter("1", false)
				.getResultList();
	}

	public void AddCommittee(Committee c) {
		em.persist(c);
	}

}
