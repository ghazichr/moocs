package moocs.ejb.bisness;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Trainer;
import moocs.ejb.entity.User;

@Stateless
public class UserManagement implements IUserRemote,IUserLocal {

	@PersistenceContext
	EntityManager em;

	public void addUser(User U) {
		em.persist(U);

	}

	public User affUser(User U) {

		return em.find(User.class, U.getIdUser());
	}
	
	
	public User  affUser(int id){
		return (User) em.createQuery("selet u from user u where u.idUser=?1")
		.setParameter("1", id)
		.getSingleResult();	
	}

	public List<User> ListAllUser() {
		
		return em.createQuery("select u from User as u").getResultList();
		
	}
	
	public User authenticate(String email, String password) {
		User found = null;
		String req ="select u from User u where u.email=:email and u.password=:password";
		TypedQuery<User> query = em.createQuery(req,User.class);
		query.setParameter("email", email);
		query.setParameter("password", password);
		found = query.getSingleResult();
		System.out.println(found.getEmail());
		return found;
	
	}

	
	
	public void updateUser(User U) {
		em.merge(U);

	}

	public void removeUser(User U) {

		em.remove(em.merge(U));

	}

}
