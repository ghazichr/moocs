package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.User;
@Local
public interface IUserLocal {
	public void  addUser(User U);
	public User  affUser(User U);
	public User  affUser(int id);
	List<User> ListAllUser ();
	public void updateUser(User U) ;
	public void removeUser(User U) ;
	public User authenticate(String email, String password);
}
