package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Remote;

import moocs.ejb.entity.Answer;
import moocs.ejb.entity.Question;


@Remote
public interface IAnswerRemote {

	void addAnswer(Answer a);
	Answer affAnswer(Answer a);
	void update(Answer a);
	void remove(Answer a);
	List<Answer> afficherQuestionAnswers(Question q);
}
