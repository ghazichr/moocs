package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.CertificatLearner;
import moocs.ejb.entity.CertificatLearnerPK;
import moocs.ejb.entity.Certificate;
import moocs.ejb.entity.Examen;
import moocs.ejb.entity.Learner;
@Stateless
public class CertificateLearnerManagement implements ICertificateLearnerLocal {
	@PersistenceContext
	EntityManager em;
	@Override
	public void addCertificateLearner(Certificate b, Learner l, int note) {
		CertificatLearner CL =new CertificatLearner();
		CertificatLearnerPK CLPK = new CertificatLearnerPK();
		CLPK.setIdCertificate(b.getIdCertificate());
		CLPK.setIdUser(l.getIdUser());
		CL.setPk(CLPK);
		CL.setNote(note);
		em.persist(CL);		
		
	}


}
