package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Chapter;
import moocs.ejb.entity.Course;
@Local
public interface IChapterLocal {
	public List<Chapter> afficherCourseChapter(Course q);
	public Chapter affAnswer(Chapter a);
	public void addChapter(Chapter a);
	public void update(Chapter e);
	public void remove(Chapter e);
	public List<Chapter> findAll();
	public Chapter findById(int id);
}
