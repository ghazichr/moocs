package moocs.ejb.bisness;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.plaf.basic.BasicBorders.MarginBorder;

import moocs.ejb.entity.Chapter;
import moocs.ejb.entity.Course;
import moocs.ejb.entity.Qcm;

@Stateless
public class CourseSchedulingManagement implements ICourseSchedulingRemote {
	@PersistenceContext
	EntityManager em;
	
	public void addCourse(Course c) {
		em.persist(c);
		
	}
	
	public long addCourseLong(Course c) {
		em.persist(c);
		return c.getIdCourse();
		
	}
	
	public Course affCourse(Course q) {
		return em.find(Course.class,q.getIdCourse());
	}
	
	public List<Course> afficherAllCourse() {
		return em.createQuery("select q from Course")
  				.getResultList();
		}
	
	public void update(Course e) {
		em.flush();
		em.merge(e);
		
	}
	
	public void remove(Course e) {
		
		em.remove(em.merge(e));//pour changer l'etat de 
		//detached vers managed
		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> findAll() {
		return em.createQuery("select e from Course as e")
				.getResultList();
	
	}
	@Override
	public Course findById(int id) {
		Course e =em.find(Course.class,id);
		
	    return  e;
	}
}
