package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import moocs.ejb.entity.Course;
@Stateless
public class CourseManagement implements ICourseManagement,ICourseManagementLocal {
	@PersistenceContext
	EntityManager em;
	public void validateCourse(Course c, boolean enable) {
		c.setEnable(enable);
		em.merge(c);
	}

	public List<Course> FindAllCourse0() {
		
		return em.createQuery("select c from Course as c where c.enable= ?1")
		.setParameter("1",false)
		.getResultList();
		}
	
	
	public List<Course> FindAllPopularedCourse(){
		return em.createQuery("select c from Course as c where c.note > ?1")
				.setParameter("1",4).getResultList();
		
	}
	
	
	public void addCourse(Course c) {
		em.persist(c);
		
	}
	public Course findCourse(Course c) {
		return em.find(Course.class,c.getIdCourse());		
	}

	@Override
	public List<Course> findallCourseByUser(int idT) {
		
		 return  em.createQuery("select c from Course as c where c.trainer.id=?1").setParameter(1,idT).getResultList();
	}
	
}
