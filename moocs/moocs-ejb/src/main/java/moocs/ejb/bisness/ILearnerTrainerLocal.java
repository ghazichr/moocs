package moocs.ejb.bisness;

import javax.ejb.Local;

import moocs.ejb.entity.Learner;

@Local
public interface ILearnerTrainerLocal {
	public void propTrainer(Learner l , boolean demande, String CvurlDemande );
	
}
