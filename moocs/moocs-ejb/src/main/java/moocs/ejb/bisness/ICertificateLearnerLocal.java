package moocs.ejb.bisness;

import java.util.List;

import javax.ejb.Local;

import moocs.ejb.entity.Certificate;
import moocs.ejb.entity.Learner;

@Local
public interface ICertificateLearnerLocal {
	public void addCertificateLearner(Certificate b,Learner l,int note);
	
}
