package moocs.ejb.entity;

import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Badge implements Serializable {
	@Id
	@Column(name = "idBadge")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)

	int idBadge;
	

	@OneToOne(cascade=CascadeType.ALL)
	private Quizz quizz;
	private String nameBadge;
	private Blob image;
	
	@OneToMany(mappedBy="b",cascade = CascadeType.ALL )	
	private List<BadgeLearne>BadgeLearner;
	
	public int getIdBadge() {
		return idBadge;
	}
	public void setIdBadge(int idBadge) {
		this.idBadge = idBadge;
	}
	public Quizz getQuizz() {
		return quizz;
	}
	public void setQuizz(Quizz quizz) {
		this.quizz = quizz;
	}
	public String getNameBadge() {
		return nameBadge;
	}
	public void setNameBadge(String nameBadge) {
		this.nameBadge = nameBadge;
	}
	public Blob getImage() {
		return image;
	}
	public void setImage(Blob image) {
		this.image = image;
	}
	public Badge() {
		this.image=null ;
		
	}
	public Badge(int id, String nom) {
		this.idBadge=id;
		this.nameBadge=nom;
		this.image=null ;
		
	}
	public Badge(String nom){
		this.nameBadge=nom ;
		this.image=null ;
	}
	public List<BadgeLearne> getBadgeLearner() {
		return BadgeLearner;
	}
	public void setBadgeLearner(List<BadgeLearne> badgeLearner) {
		BadgeLearner = badgeLearner;
	}
	
}
