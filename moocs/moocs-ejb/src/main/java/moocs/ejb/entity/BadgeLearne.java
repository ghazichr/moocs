package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class BadgeLearne implements Serializable {
	
	@EmbeddedId
	private BadgeLearnerPK pk ;
	
	public BadgeLearnerPK getPk() {
		return pk;
	}


	public void setPk(BadgeLearnerPK pk) {
		this.pk = pk;
	}


	@JoinColumn(name="idBadge",insertable=false,updatable=false)
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Badge b ;
	
	@JoinColumn(name="idUser",insertable=false,updatable=false)
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Learner l ;

	private int note;
	public BadgeLearne() {
		super();
	}


	public Badge getB() {
		return b;
	}

	public void setB(Badge b) {
		this.b = b;
	}

	public Learner getL() {
		return l;
	}

	public void setL(Learner l) {
		this.l = l;
	}


	public int getNote() {
		return note;
	}


	public void setNote(int note) {
		this.note = note;
	}
	

}
