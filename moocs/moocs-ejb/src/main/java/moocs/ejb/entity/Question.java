package moocs.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Question implements Serializable {
	@Id
	@Column(name = "idQuestion")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	int idQuestion;
	String question;
	@ManyToOne(cascade = CascadeType.PERSIST )
	private Qcm qcm;

	@OneToMany(mappedBy = "question",cascade = CascadeType.ALL ,fetch=FetchType.EAGER)
	private List<Answer> answers;

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Question() {

	}

	public Question(int idQuestion, String question) {
		super();
		this.idQuestion = idQuestion;
		this.question = question;
	}

	public Qcm getQcm() {
		return qcm;
	}

	public void setQcm(Qcm qcm) {
		this.qcm = qcm;
	}
	public void Affect(List <Answer> lAnswers)
	{
		for(Answer a : lAnswers)
		{
			a.setQuestion(this);
			this.getAnswers().add(a);
		}
		
		
	}
}
