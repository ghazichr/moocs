package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Examen extends Qcm implements Serializable {

	@OneToOne(mappedBy = "examen")
	private Certificate certificate;
	private float successExamen;

	public Certificate getCertificate() {
		return certificate;
	}

	public void setCertificate(Certificate certificate) {
		this.certificate = certificate;
	}

	public float getSuccessExamen() {
		return successExamen;
	}

	public void setSuccessExamen(float successExamen) {
		this.successExamen = successExamen;
	}

	public Examen() {
		super();
	}

	public Examen(int idQcm, String name, Float success) {
		super(idQcm, name);
		this.successExamen = success;
	}

}
