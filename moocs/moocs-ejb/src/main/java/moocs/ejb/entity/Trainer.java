package moocs.ejb.entity;

import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Trainer extends User implements Serializable {

	private boolean enable;
	private String university;
	private String cvUrl;
	private int note ;
	// cette variable permet de faire la demande  pour devenir Committee !!
	private boolean demande;
	// cette variable permet de comtabilier le numbre de vote il faut 3 vote pour etre commitee
	private int voteNumbr;
	@OneToMany(mappedBy="trainer")
	private List<Course> courses ;
	
	
	private Integer rating ;

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	

	
	public Trainer(String email, String password, String lastName, String firstName, Roles role,Blob picture,String university ,int note){
		super(email, password, lastName, firstName, role, picture);
		this.university=university;
		this.note=note;
	}
	
	public Trainer(String email, String password, String lastName, String firstName, Roles role,String university ,int note){
		super(email, password, lastName, firstName, role);
		this.university=university;
		this.note=note;
	}
	
	public boolean isDemande() {
		return demande;
	}
	
	
	public void calculNote(){
		if(this.note==0)
			this.note=rating;
		else
			this.note=Math.round((this.note+this.rating)/2);
		
	}

	public void setDemande(boolean demande) {
		this.demande = demande;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public Trainer() {
		super();
	}

	public String getCvUrl() {
		return cvUrl;
	}

	public void setCvUrl(String cvUrl) {
		this.cvUrl = cvUrl;
	}

	public int getVoteNumbr() {
		return voteNumbr;
	}

	public void setVoteNumbr(int voteNumbr) {
		this.voteNumbr = voteNumbr;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}
	

}
