package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
@Embeddable
public class CertificatLearnerPK implements Serializable{
	
	private int idCertificate ;
	private int idUser;
	public CertificatLearnerPK() {
		super();
	}
	public CertificatLearnerPK(int idCertificate, int idUser) {
		super();
		this.idCertificate = idCertificate;
		this.idUser = idUser;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCertificate;
		result = prime * result + idUser;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CertificatLearnerPK other = (CertificatLearnerPK) obj;
		if (idCertificate != other.idCertificate)
			return false;
		if (idUser != other.idUser)
			return false;
		return true;
	}
	public int getIdCertificate() {
		return idCertificate;
	}
	public void setIdCertificate(int idCertificate) {
		this.idCertificate = idCertificate;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	
}
