package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Answer implements Serializable {
	@Id
	@Column(name = "idAnswer")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	int idAnswer;
	String answer;
	boolean verification;
	int count;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Question question;

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public int getIdAnswer() {
		return idAnswer;
	}

	public void setIdAnswer(int idAnswer) {
		this.idAnswer = idAnswer;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public boolean isVerification() {
		return verification;
	}

	public void setVerification(boolean verification) {
		this.verification = verification;
	}

	public Answer() {

	}

	public Answer(int idAnswer, String answer, boolean verification) {
		super();
		this.idAnswer = idAnswer;
		this.answer = answer;
		this.verification = verification;
	}

}
