package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class BadgeLearnerPK  implements Serializable{
	
	private int idBadge ;
	private int idUser;
	public BadgeLearnerPK() {
		super();
	}
	public BadgeLearnerPK(int idBadge, int idUser) {
		super();
		this.idBadge = idBadge;
		this.idUser = idUser;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idBadge;
		result = prime * result + idUser;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BadgeLearnerPK other = (BadgeLearnerPK) obj;
		if (idBadge != other.idBadge)
			return false;
		if (idUser != other.idUser)
			return false;
		return true;
	}
	public int getIdBadge() {
		return idBadge;
	}
	public void setIdBadge(int idBadge) {
		this.idBadge = idBadge;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	
}
