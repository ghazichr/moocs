package moocs.ejb.entity;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Chapter implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int idChapter;
	private String urlVideo;
	private String title;
	private String contained;
	private String urlPdf;
	private String description;
	private Blob pic1;
	private Blob pic2;
	@ManyToOne
	private Course course;

	public int getIdChapter() {
		return idChapter;
	}

	public void setIdChapter(int idChapter) {
		this.idChapter = idChapter;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContained() {
		return contained;
	}

	public void setContained(String contained) {
		this.contained = contained;
	}

	public String getUrlPdf() {
		return urlPdf;
	}

	public void setUrlPdf(String urlPdf) {
		this.urlPdf = urlPdf;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Blob getPic1() {
		return pic1;
	}

	public void setPic1(Blob pic1) {
		this.pic1 = pic1;
	}

	public Blob getPic2() {
		return pic2;
	}

	public void setPic2(Blob pic2) {
		this.pic2 = pic2;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

}
