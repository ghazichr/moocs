package moocs.ejb.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Course implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int idCourse;
	private String title;
	private String urlVideo;
	private String description;
	private String category;
	private String urlPdf;
	private boolean enable;
	private boolean updateCourse;
	private String startDate;
	private String endDate;
	private int note ;
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@ManyToOne
	private Trainer trainer ;
	@OneToMany(mappedBy="course")
	private List<Chapter> chapters;
	public int getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(int idCourse) {
		this.idCourse = idCourse;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrlVideo() {
		return urlVideo;
	}
	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public Course(){
		
	}
	
	
	public Course(int idCourse, String title, String urlVideo, String description, String category, String urlPdf,
			boolean enable, boolean updateCourse, String startDate, String endDate, Trainer trainer,
			List<Chapter> chapters) {
		super();
		this.idCourse = idCourse;
		this.title = title;
		this.urlVideo = urlVideo;
		this.description = description;
		this.category = category;
		this.urlPdf = urlPdf;
		this.enable = enable;
		this.updateCourse = updateCourse;
		this.startDate = startDate;
		this.endDate = endDate;
		this.trainer = trainer;
		this.chapters = chapters;
	}
	public String getUrlPdf() {
		return urlPdf;
	}
	public void setUrlPdf(String urlPdf) {
		this.urlPdf = urlPdf;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public boolean isUpdateCourse() {
		return updateCourse;
	}
	public void setUpdateCourse(boolean updateCourse) {
		this.updateCourse = updateCourse;
	}
	public Trainer getTrainer() {
		return trainer;
	}
	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}
	public List<Chapter> getChapters() {
		return chapters;
	}
	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}
	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}

}
