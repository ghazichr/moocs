package moocs.ejb.entity;


import java.io.Serializable;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;


@Entity
public class Learner extends User implements Serializable {

	
	private boolean enable;
	// cette variable permet de faire la demande pour devenir trainer
	private boolean demande;
	// cette variable contient url du CV de la demande pour devenir trainer
	private String CvurlDemande;
	
	@OneToMany(mappedBy="l",cascade = CascadeType.ALL )	
	private List<BadgeLearne>BadgeLearner;
	
	@OneToMany(mappedBy="l",cascade = CascadeType.ALL )	
	private List<CertificatLearner> CertificateLearner;

		public Learner(){
			
		}
		
		
		
		
	public Learner(String email, String password, String lastName, String firstName, Roles role,String cv){
		super(email, password, lastName, firstName, role);
		CvurlDemande = cv ;
	}
	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isDemande() {
		return demande;
	}

	public void setDemande(boolean demande) {
		this.demande = demande;
	}

	public String getCvurlDemande() {
		return CvurlDemande;
	}

	public void setCvurlDemande(String cvurlDemande) {
		CvurlDemande = cvurlDemande;
	}




	public List<BadgeLearne> getBadgeLearner() {
		return BadgeLearner;
	}




	public void setBadgeLearner(List<BadgeLearne> badgeLearner) {
		BadgeLearner = badgeLearner;
	}




	public List<CertificatLearner> getCertificateLearner() {
		return CertificateLearner;
	}




	public void setCertificateLearner(List<CertificatLearner> certificateLearner) {
		CertificateLearner = certificateLearner;
	}




	









	
	
	


      
}
