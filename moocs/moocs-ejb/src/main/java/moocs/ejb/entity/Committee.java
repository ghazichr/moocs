package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Committee extends User implements Serializable {
private int voteNumbr ;
private boolean enable ;



public Committee(){
	
}

public Committee(String email, String password, String lastName, String firstName, Roles role){
	super(email, password, lastName, firstName, role);

	
}

public int getVoteNumbr() {
	return voteNumbr;
}
public void setVoteNumbr(int voteNumbr) {
	this.voteNumbr = voteNumbr;
}
public boolean isEnable() {
	return enable;
}
public void setEnable(boolean enable) {
	this.enable = enable;
}

}
