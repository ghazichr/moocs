package moocs.ejb.entity;

import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Certificate implements Serializable {
	@Id
	@Column(name = "idCertificate")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)

	int idCertificate;

	@OneToOne(cascade = CascadeType.ALL)
	private Examen examen;
	
	@OneToMany(mappedBy="b",cascade = CascadeType.ALL )	
	private List<CertificatLearner>CertificatLearner;
	
	private String nameCertificate;
	private Blob image;
	
	public int getIdCertificate() {
		return idCertificate;
	}
	public void setIdCertificate(int idCertificate) {
		this.idCertificate = idCertificate;
	}
	public Examen getExamen() {
		return examen;
	}
	public void setExamen(Examen examen) {
		this.examen = examen;
	}
	public String getNameCertificate() {
		return nameCertificate;
	}
	public void setNameCertificate(String nameCertificate) {
		this.nameCertificate = nameCertificate;
	}
	public Blob getImage() {
		return image;
	}
	public void setImage(Blob image) {
		this.image = image;
	}
	public Certificate() {
		super();
	}
	public Certificate(int id, String nom) {
		this.idCertificate=id;
		this.nameCertificate=nom;
		
	}
	public List<CertificatLearner> getCertificatLearner() {
		return CertificatLearner;
	}
	public void setCertificatLearner(List<CertificatLearner> certificatLearner) {
		CertificatLearner = certificatLearner;
	}
	
}
