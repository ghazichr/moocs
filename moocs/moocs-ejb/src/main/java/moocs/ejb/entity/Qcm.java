package moocs.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
/*
 * The QCM is the Survey Entity . 
 * nothing differentiate both entities */

@Entity
@Inheritance(strategy =InheritanceType.JOINED)
public class Qcm implements Serializable{
	@Id
	@Column(name = "idQcm")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)

	int idQcm;
	String name;
	
	@OneToMany(mappedBy="qcm" ,cascade = CascadeType.ALL ,fetch=FetchType.EAGER)
	List<Question> questions ;

	public int getIdQcm() {
		return idQcm;
	}

	public void setIdQcm(int idQcm) {
		this.idQcm = idQcm;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Qcm() {

	}

	public Qcm(int idQcm, String name) {
		super();
		this.idQcm = idQcm;
		this.name = name;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	@Override
	public String toString() {
		return "Qcm [id=" + idQcm + ", nom_Qcm=" +name+ "]";
	}
	public void Affect(List <Question> lQuestions)
	{
		for(Question q : lQuestions)
		{
			q.setQcm(this);
			this.getQuestions().add(q);
		}
		
		
	}
}
