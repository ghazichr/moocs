package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Quizz extends Qcm implements Serializable {

	
	@OneToOne(mappedBy = "quizz")
	private Badge badge;
	private float successQuizz;
	public Badge getBadge() {
		return badge;
	}
	public void setBadge(Badge badge) {
		this.badge = badge;
	}
	
	
	public float getSuccessQuizz() {
		return successQuizz;
	}
	public void setSuccessQuizz(float successQuizz) {
		this.successQuizz = successQuizz;
	}
	public Quizz() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Quizz(int idQcm, String name , float success) {
		super(idQcm, name);
		this.successQuizz=success;
	}
	
	}
