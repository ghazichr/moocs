package moocs.ejb.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class CertificatLearner implements Serializable {
	
	@EmbeddedId
	private CertificatLearnerPK pk ;
	
	public CertificatLearnerPK getPk() {
		return pk;
	}


	public void setPk(CertificatLearnerPK pk) {
		this.pk = pk;
	}


	@JoinColumn(name="idCertificate",insertable=false,updatable=false)
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Certificate b ;
	
	@JoinColumn(name="idUser",insertable=false,updatable=false)
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Learner l ;

	private int note;
	
	public CertificatLearner() {
		super();
	}


	public Certificate getB() {
		return b;
	}

	public void setB(Certificate b) {
		this.b = b;
	}

	public Learner getL() {
		return l;
	}

	public void setL(Learner l) {
		this.l = l;
	}


	public int getNote() {
		return note;
	}


	public void setNote(int note) {
		this.note = note;
	}
	

}
