package moocs.util;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import moocs.ejb.bisness.IBadgeLearnerLocal;
import moocs.ejb.bisness.ICommitteeManagementLocal;
import moocs.ejb.bisness.ILearnerLocal;
import moocs.ejb.bisness.ITrainerLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.bisness.IbadgeLocal;
import moocs.ejb.entity.Badge;
import moocs.ejb.entity.Committee;
import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Roles;
import moocs.ejb.entity.Trainer;
import moocs.ejb.entity.User;

@Singleton
@Startup
public class DBPopulator {
	@EJB
	private IUserLocal userloc ;
	@EJB
	private ILearnerLocal learnerloc ;
	@EJB
	private ITrainerLocal trainerloc ;
	@EJB
	private IbadgeLocal badgeloc ;
	@EJB
	private IBadgeLearnerLocal bloc ;
	
	@EJB
	private ICommitteeManagementLocal com ;
	
	public DBPopulator(){
		
	}

	
	@PostConstruct
	public void createDate(){
		
		Committee comm = new Committee("mima","000","touka","pital", Roles.Committee );
		
		User user = new User("seif.amouchi@gmail.com","123","seif","amouchi",Roles.Learner);
		Learner learner = new Learner("ahmed@gmail.com","1234","ah","med",Roles.Learner,"cvurm");
		Trainer trainer = new Trainer("biba@ganouti","12345","amchoun","fa7moun",Roles.Trainer,"fst",3);

		Trainer trainer1 = new Trainer("tikourdou@","123a","loco","dice",Roles.Trainer,"fst",4);
		Trainer trainer2= new Trainer("hamadi@harm","12a","rahn","rateb",Roles.Trainer,"fst",2);

		Trainer trainer3 = new Trainer("ganour","345","choun","moun",Roles.Trainer,"fst",0);
		Trainer trainer4 = new Trainer("bib","12","am","fa",Roles.Trainer,"fst",3);
		Badge badge = new Badge("stifa");
		Badge badge2 = new Badge("sarsar");
		userloc.addUser(user);
		learnerloc.addLearner(learner);
		trainerloc.addTrainer(trainer);
		trainerloc.addTrainer(trainer1);
		trainerloc.addTrainer(trainer2);
		trainerloc.addTrainer(trainer3);
		trainerloc.addTrainer(trainer4);
		badgeloc.addBadge(badge);
		badgeloc.addBadge(badge2);
		bloc.addBadgeLearner(badge2, learner, 5);
		com.AddCommittee(comm);
		
		
		
	}
}
