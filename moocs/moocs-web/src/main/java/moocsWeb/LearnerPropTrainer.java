package moocsWeb;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.UploadedFile;

import com.dropbox.core.DbxException;

import Extra.DropBox;
import moocs.ejb.bisness.ILearnerLocal;
import moocs.ejb.bisness.ILearnerTrainerLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.entity.Learner;

@ManagedBean
@ViewScoped
public class LearnerPropTrainer {
	
@EJB
ILearnerTrainerLocal LTL;
private UploadedFile upf ;
private boolean Demande;
@EJB
IUserLocal IUL;
@EJB
ILearnerLocal ILL;




public UploadedFile getUpf() {
	return upf;
}
public void setUpf(UploadedFile upf) {
	this.upf = upf;
}
public boolean isDemande() {
	return Demande;
}
public void setDemande(boolean demande) {
	Demande = demande;
}
public void addDemandeTrainer() throws IOException{
	DropBox drop = new DropBox();
	String name =upf.getFileName();
	String extension=upf.getContentType();
	drop.setTest(1);
	drop.setIS(upf.getInputstream());
	drop.setLearner("/learner/"+name);
	try {
		drop.UpLoad();
	} catch (DbxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Learner l=new Learner();
	HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
	Authentification auth=(Authentification) req.getSession().getAttribute("authentification");
l.setIdUser(IUL.authenticate(auth.getEmail(), auth.getPassword()).getIdUser());
l=ILL.affLearner(l);
l.setCvurlDemande("/learner/"+name);
l.setDemande(true);
	LTL.propTrainer(l, true, "/learner/"+name);
	
}
}
