package moocsWeb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RateEvent;

import moocs.ejb.bisness.ITrainerLocal;
import moocs.ejb.entity.Badge;
import moocs.ejb.entity.Trainer;

@ManagedBean
@SessionScoped
public class TrainerBean {
	
	@EJB
	ITrainerLocal ser ;
	private String name ;
	private int id ;
	private String firstname ;
	private String lastname ;
	private String email ;
	private List<Trainer> trainers ;
	private List<Trainer> trainersRech ;
	private List<Trainer> trainersPop ;
	private boolean list_aff = true;
	private Trainer selectedTrainer ;
	private Integer rating ;
	private int note ;

	
	@PostConstruct
	public void init(){
		selectedTrainer= new Trainer() ;
		
	}
	
	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	
public String showSearch(){
		
		return "/trainer/listTrainerRecherche?faces-redirect=true";
	}

	
	public String showProfile(){
		
		return "/trainer/profileTrainer?faces-redirect=true";
	}
	
	
	public String showCourseTrainer(){
		
		
		return "/course/displayCoursesLearn?faces-redirect=true";
	}
	
	public String doNote(){
		
		if (selectedTrainer.getNote()==0)
			note=rating;
		else 
		{
			float a = (selectedTrainer.getNote()+rating)/2 ;
			note = (int) a ;
		System.out.println(selectedTrainer.getNote());
		}
		selectedTrainer.setNote(note);
		ser.updateTrainer(selectedTrainer);
		
		return "/trainer/profileTrainer?faces-redirect=true" ;
	}
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Trainer> getTrainers() {
		trainers=ser.listalltrainer();
		return trainers;
	}
	public void setTrainers(List<Trainer> trainers) {
		this.trainers = trainers;
	}
	public List<Trainer> getTrainersRech() {
		trainersRech=ser.findAllTrainerName(name);
		return trainersRech;
	}
	public void setTrainersRech(List<Trainer> trainersRech) {
		this.trainersRech = trainersRech;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isList_aff() {
		return list_aff;
	}
	public void setList_aff(boolean list_aff) {
		this.list_aff = list_aff;
	}
	
	public Trainer getSelectedTrainer() {
		return selectedTrainer;
	}
	public void setSelectedTrainer(Trainer selectedTrainer) {
		this.selectedTrainer = selectedTrainer;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public List<Trainer> getTrainersPop() {
		trainersPop=ser.findPopularTrainer();
		return trainersPop;
	}

	public void setTrainersPop(List<Trainer> trainersPop) {
		this.trainersPop = trainersPop;
	}
	
	
	
	

}
