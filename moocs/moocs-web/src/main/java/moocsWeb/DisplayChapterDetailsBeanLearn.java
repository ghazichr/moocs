package moocsWeb;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import moocs.ejb.entity.Chapter;

@ManagedBean
@ViewScoped
public class DisplayChapterDetailsBeanLearn {
	

     
	private Chapter chapter;
	@PostConstruct
	public void  init() {
		chapter=new Chapter();
		
	}
	
	public Chapter getChapter() {
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		DisplayChaptersBeanLearn dcb=(DisplayChaptersBeanLearn) req.getSession().getAttribute("displayChaptersBeanLearn");
		chapter=dcb.getChapter();
		
		return chapter;
	}
	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}
	

}
