package moocsWeb;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.primefaces.model.UploadedFile;

import moocs.ejb.bisness.IBadgeLearnerLocal;
import moocs.ejb.bisness.ICertificateLearnerLocal;
import moocs.ejb.bisness.ICertificateLocal;
import moocs.ejb.bisness.IExamenLocal;
import moocs.ejb.bisness.IQuestionLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.entity.Answer;
import moocs.ejb.entity.Certificate;
import moocs.ejb.entity.Examen;
import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;

@ManagedBean
@SessionScoped
public class ExamenBean {

	@EJB
	IExamenLocal serExamen;
	@EJB
	IQuestionLocal serQuestion;
	@EJB
	ICertificateLocal ser;
	@EJB
	ICertificateLearnerLocal serBL;
	@EJB
	IUserLocal serUser;
	private String nameCertificate;
	private UploadedFile file1;
	private List<Examen> Examens;
	private List<Question> questions;
	private List<Learner> learners;
	private List<Answer> answers;
	private Examen qui;
	private List<Answer> chosen;
	private int ExamenScore;
	private String name;
	private int note;
	private int qnumber;
	private boolean displayQuestion;
	private boolean displayall = true;
	private boolean displayUpdate;
	private boolean displayExamen;
	private boolean displayScore;
	private List<String> mychoix = new ArrayList<String>();

	@PostConstruct
	public void init() {
		questions = new ArrayList<Question>();
		for (int i = 0; i < 20; i++) {
			mychoix.add("");
		}

	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public int getQnumber() {
		return qnumber;

	}

	public void setQnumber(int qnumber) {
		this.qnumber = qnumber;
	}

	public String doDelete() {
		serExamen.remove(qui);
		return "/examen/trainer/list?faces-redirect=true";
	}


	public String doUpdate() {
		serExamen.update(qui);
		return "/examen/trainer/list?faces-redirect=true";
	}
	public String doRenderTake()
	{
		return"/examen/learner/takeExamen?faces-redirect=true";
	}

	public String doTake() {
		ExamenScore = 0;
		for (String g : mychoix) {
			if (g.equals("true")) {
				ExamenScore++;
			}
		}
		if (ExamenScore>=note)
		{
			HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			Authentification auth=(Authentification) req.getSession().getAttribute("authentification");
			serBL.addCertificateLearner(qui.getCertificate(), (Learner)serUser.authenticate(auth.getEmail(),auth.getPassword()), ExamenScore);
		}
		return "/examen/trainer/list?faces-redirect=true";
	}

	public String doAdd() throws IOException, SerialException, SQLException {
		Examen q = new Examen();
		q.setName(name);
		q.setSuccessExamen(note);
		q.setQuestions(questions);

		serExamen.addExamen(q);
		q = serExamen.affExamen(q);
		Qcm qcm = serExamen.affQcm(q);
		for (Question quest : questions) {

			quest.setQcm(qcm);
			quest = serQuestion.affQuestion(quest);
		}
		q.setQuestions(questions);
		for (Question quest : questions) {
			answers = quest.getAnswers();
			for (Answer anser : answers) {
				anser.setQuestion(quest);
			}
			quest.setAnswers(answers);
		}
		serExamen.update(q);
		Certificate b1 = new Certificate();
		b1.setNameCertificate(nameCertificate);
		b1.setImage(ConverteBYTEtoBloob(file1.getContents()));
		ser.addCertificate(b1);
		b1 = ser.affCertificate(b1);
		b1.setExamen(q);
		ser.update(b1);
		return "/examen/trainer/list?faces-redirect=true";

	}

	private Blob ConverteBYTEtoBloob(byte[] contents) throws SerialException, SQLException {

		return new SerialBlob(contents);
	}

	public String displayQuestion() {
		displayQuestion = true;

		for (int i = 0; i < qnumber; i++) {
			Question q = new Question();
			answers = new ArrayList<Answer>();
			Answer a1 = new Answer();
			Answer a2 = new Answer();
			Answer a3 = new Answer();
			Answer a4 = new Answer();
			answers.add(a1);
			answers.add(a2);
			answers.add(a3);
			answers.add(a4);
			q.setAnswers(answers);
			questions.add(q);

		}
		return null;
	}

	public boolean isDisplayQuestion() {
		return displayQuestion;
	}

	public void setDisplayQuestion(boolean displayQuestion) {
		this.displayQuestion = displayQuestion;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDisplayall() {
		return displayall;
	}

	public void setDisplayall(boolean displayall) {
		this.displayall = displayall;
	}

	public boolean isDisplayUpdate() {
		return displayUpdate;
	}

	public void setDisplayUpdate(boolean displayUpdate) {
		this.displayUpdate = displayUpdate;
	}

	public List<Examen> getExamens() {
		Examens = serExamen.afficherAllExamen();
		return Examens;
	}

	public void setExamens(List<Examen> Examens) {
		this.Examens = Examens;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public Examen getQui() {
		return qui;
	}

	public void setQui(Examen qui) {
		this.qui = qui;
	}

	public boolean isDisplayExamen() {
		return displayExamen;
	}

	public void setDisplayExamen(boolean displayExamen) {
		this.displayExamen = displayExamen;
	}

	public List<Answer> getChosen() {
		return chosen;
	}

	public void setChosen(List<Answer> chosen) {
		this.chosen = chosen;
	}

	public int getExamenScore() {
		return ExamenScore;
	}

	public void setExamenScore(int ExamenScore) {
		this.ExamenScore = ExamenScore;
	}

	public boolean isDisplayScore() {
		return displayScore;
	}

	public void setDisplayScore(boolean displayScore) {
		this.displayScore = displayScore;
	}

	public List<String> getMychoix() {
		return mychoix;
	}

	public void setMychoix(List<String> mychoix) {
		this.mychoix = mychoix;
	}

	public UploadedFile getFile1() {
		return file1;
	}

	public void setFile1(UploadedFile file1) {
		this.file1 = file1;
	}

	public String getnameCertificate() {
		return nameCertificate;
	}

	public void setnameCertificate(String nameCertificate) {
		this.nameCertificate = nameCertificate;
	}

	public List<Learner> getLearners() {
		return learners;
	}

	public void setLearners(List<Learner> learners) {
		this.learners = learners;
	}

}