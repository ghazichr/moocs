package moocsWeb;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.UploadedFile;

import moocs.ejb.bisness.IbadgeLocal;
import moocs.ejb.entity.Badge;

@ManagedBean
@ViewScoped
public class BadgeBean {
	
	@EJB
	IbadgeLocal ser ;
	private int id ;
	private String nameBadge ;
	private UploadedFile file1;
	private Badge selectedBadge ;
	private boolean list_aff = true;
	private boolean show_Update = false;
	private java.util.List<Badge> badges ;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	@PostConstruct
	public void init(){
		
	}
	
	public String doAdd()
	{
		
		Badge b1 = new Badge();
		b1.setNameBadge(nameBadge);
		b1.setImage(null);
		ser.addBadge(b1);
		
		return null ;
	}
	
	public String doUpdate(){
		ser.update(selectedBadge);
		return null;
	}
	
	
	public String doDel(){
		Badge b = new Badge();
		ser.remove(selectedBadge);
		return null ;
	}

	public String getNameBadge() {
		return nameBadge;
	}
	public void setNameBadge(String nameBadge) {
		this.nameBadge = nameBadge;
	}
	public java.util.List<Badge> getBadges() {
		badges=ser.affAllBadges();
		return badges;
	}
	public void setBadges(java.util.List<Badge> badges) {
		this.badges = badges;
	}
	public boolean isList_aff() {
		return list_aff;
	}
	public void setList_aff(boolean list_aff) {
		this.list_aff = list_aff;
	}
	public boolean isShow_Update() {
		return show_Update;
	}
	public void setShow_Update(boolean show_Update) {
		this.show_Update = show_Update;
	}
	public Badge getSelectedBadge() {
		return selectedBadge;
	}
	public void setSelectedBadge(Badge selectedBadge) {
		this.selectedBadge = selectedBadge;
	}
	public UploadedFile getFile1() {
		return file1;
	}
	public void setFile1(UploadedFile file1) {
		this.file1 = file1;
	}
	
	
	
}
