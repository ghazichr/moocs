package moocsWeb;

import java.awt.List;


import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import moocs.ejb.bisness.ICertificateLocal;
import moocs.ejb.entity.Badge;
import moocs.ejb.entity.Certificate;

@ManagedBean
@ViewScoped
public class CertifBean {
	
	@EJB
	ICertificateLocal ser ;
	
	
	private int id ;
	private String name ;
	private boolean list_aff = true;
	private Certificate selectedCert ;
	private boolean show_Update = false;
	private java.util.List<Certificate> certifs ;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isList_aff() {
		return list_aff;
	}
	public void setList_aff(boolean list_aff) {
		this.list_aff = list_aff;
	}
	public Certificate getSelectedCert() {
		return selectedCert;
	}
	public void setSelectedCert(Certificate selectedCert) {
		this.selectedCert = selectedCert;
	}
	public boolean isShow_Update() {
		return show_Update;
	}
	public void setShow_Update(boolean show_Update) {
		this.show_Update = show_Update;
	}
	public java.util.List<Certificate> getCertifs() {
		certifs =ser.affAllCertifs();
		return certifs;
	}
	public void setCertifs(java.util.List<Certificate> certifs) {
		
		this.certifs = certifs;
	}
	
	public String doAdd()
	{
		
		Certificate C = new Certificate();
		C.setNameCertificate(name);
		C.setImage(null);
		ser.addCertificate(C);
		
		return null ;
	}
	
	public String doUpdate(){
		ser.update(selectedCert);
		return null;
	}
		public String doDel(){
			ser.remove(selectedCert);
			return null ;
		}
	}
	
	
	
	
	
	

