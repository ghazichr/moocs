package moocsWeb;


import java.io.IOException;

import java.sql.Blob;
import java.sql.SQLException;

import java.util.ArrayList;

import java.util.List;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;



import org.primefaces.model.UploadedFile;

import moocs.ejb.bisness.IChapterLocal;
import moocs.ejb.bisness.ICourseManagementLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.entity.Chapter;
import moocs.ejb.entity.Course;
import moocs.ejb.entity.Trainer;

@ManagedBean
@ViewScoped
public class AddCourseBean2 {
	@EJB
	ICourseManagementLocal ICML;
	@EJB
	IChapterLocal ICL;
	@EJB
	IUserLocal IUL;

	private String titel;
	private String category;
	private String description;
	private String urlpdf;
	private String urlVideo;
	private String enddate;
	private String startdate;
	private int chaptersnum;
	private int chapternum1;
	private boolean display;
	private ArrayList<Chapter> chapters;
	private Chapter Chapt;
	private String login;
	private String password;
	
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private ArrayList<UploadedFile> file1;
	private ArrayList<UploadedFile> file2;
	
	
	public ArrayList<UploadedFile> getFile1() {
		return file1;
	}

	public void setFile1(ArrayList<UploadedFile> file1) {
		this.file1 = file1;
	}

	public ArrayList<UploadedFile> getFile2() {
		return file2;
	}

	public void setFile2(ArrayList<UploadedFile> file2) {
		this.file2 = file2;
	}

	public Chapter getChapt() {
		return Chapt;
	}

	public void setChapt(Chapter chapt) {
		Chapt = chapt;
	}

	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(ArrayList<Chapter> chapters) {
		this.chapters = chapters;
	}

	@PostConstruct
	public void init() {
		setDisplay(false);
		chapters = new ArrayList<Chapter>();
		file1=new ArrayList<UploadedFile>();
		file2=new ArrayList<UploadedFile>();
	}

	public int getChapternum1() {
		return chapternum1;
	}

	public void setChapternum1(int chapternum1) {
		this.chapternum1 = chapternum1;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	public int getChaptersnum() {
		return chaptersnum;
	}

	public void setChaptersnum(int chaptersnum) {
		this.chaptersnum = chaptersnum;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrlpdf() {
		return urlpdf;
	}

	public void setUrlpdf(String urlpdf) {
		this.urlpdf = urlpdf;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String addCourse() throws IOException, SerialException, SQLException {
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Authentification auth=(Authentification) req.getSession().getAttribute("authentification");
		Course c = new Course();
		c.setTrainer((Trainer)IUL.authenticate(auth.getEmail(),auth.getPassword()));
		c.setCategory(category);
		c.setDescription(description);
		c.setUrlPdf(urlpdf);
		c.setUrlVideo(urlVideo);
		c.setTitle(titel);
		ICML.addCourse(c);

		Course cc = new Course();
		cc = ICML.findCourse(c);
		for (int i = 0; i < chapters.size(); i++) {
			chapters.get(i).setPic1(ConverteBYTEtoBloob(file1.get(i).getContents()));
			chapters.get(i).setPic2(ConverteBYTEtoBloob(file2.get(i).getContents()));

		}

		for (Chapter chapts : chapters) {

			chapts.setCourse(cc);
			ICL.addChapter(chapts);
		}

		return null;
	}

	private Blob ConverteBYTEtoBloob(byte[] contents) throws SerialException, SQLException {
		
		return new SerialBlob(contents);
	}

	public String displayChapter() {
		display = true;
		chapternum1 = chaptersnum;

		for (int i = 0; i < chaptersnum; i++) {
			Chapter c = new Chapter();
			UploadedFile uf=null;
			c.setContained("");
			chapters.add(c);
			file1.add(uf);
			file2.add(uf);

		}
		return null;
	}

}
