package moocsWeb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import Extra.DropBox;
import moocs.ejb.bisness.ITrainerComiteeLocal;
import moocs.ejb.bisness.ITrainerLocal;
import moocs.ejb.entity.Trainer;

@ManagedBean
@ViewScoped
public class CommitteeBean {
	@EJB
	ITrainerLocal ITL;
	private List<Trainer> trainers;
	private int id;
	private String cv;
	private Trainer t;
	@PostConstruct
	public void init (){
		trainers=ITL.findAllTrainerDemande1();
		
	}
	public String voterPour(){
		
		ITL.validateTrainer(t, true);
		trainers=ITL.findAllTrainerDemande1();
		return null;
	}
	
	public String download(){
		DropBox drop=new DropBox();
		drop.downloadfiledrop(t.getCvUrl());
		
		return null;
	}
	
	public Trainer getT() {
		return t;
	}
	public void setT(Trainer t) {
		this.t = t;
	}
	public ITrainerLocal getITL() {
		return ITL;
	}
	public void setITL(ITrainerLocal iTL) {
		ITL = iTL;
	}
	public List<Trainer> getTrainers() {
		return trainers;
	}
	public void setTrainers(List<Trainer> trainers) {
		this.trainers = trainers;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	
	
	

}
