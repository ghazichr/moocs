package moocsWeb;



import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.entity.Committee;
import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Roles;
import moocs.ejb.entity.Trainer;
import moocs.ejb.entity.User;



@ManagedBean
@SessionScoped
public class Authentification {
private String email;
private String password ;
private String firstname;
public String getFirstname() {
	return firstname;
}
public void setFirstname(String firstname) {
	this.firstname = firstname;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
}
private String lastname;
private User loggedUser;
@EJB
IUserLocal service;
private String user_type ="";

public Authentification(){
	
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String  Login() {
	
	if (service.authenticate(email, password) instanceof Learner ){
		user_type="learner";
		loggedUser=(Learner)service.authenticate(email, password);
		return "/learner/learnerHome?faces-redirect=true";
	}
	else if (service.authenticate(email, password) instanceof Trainer ){
		user_type="trainer";
		loggedUser=(Trainer)service.authenticate(email, password);
		return "/trainer/trainerHome?faces-redirect=true";
	}
	else if (service.authenticate(email, password) instanceof Committee) {
		user_type="committee";
		loggedUser=(Committee)service.authenticate(email, password);
		return "/committee/committeeVote?faces-redirect=true";
		
	}
	else{ 
		return null ;
	
	}
	
}
public String getUser_type() {
	return user_type;
}
public void setUser_type(String user_type) {
	this.user_type = user_type;
}
public User getLoggedUser() {
	return loggedUser;
}
public void setLoggedUser(User loggedUser) {
	this.loggedUser = loggedUser;
}
}
