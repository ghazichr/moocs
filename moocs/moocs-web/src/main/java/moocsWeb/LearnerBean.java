package moocsWeb;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.jws.soap.SOAPBinding.Use;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.primefaces.model.UploadedFile;

import moocs.ejb.bisness.ILearnerLocal;
import moocs.ejb.bisness.IUserRemote;
import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Roles;
import moocs.ejb.entity.User;

@ManagedBean
@ViewScoped
public class LearnerBean {

	@EJB
	ILearnerLocal ser;
	@EJB
	IUserRemote serr;

	

	private int id;
	private String firstname;
	private String lastname;
	private UploadedFile file1=null ;
	User us;

	private String email;
	private String password;

	private boolean displayUpdate = false;
	private boolean displayAdd = false;

	private User logged = null;

	@PostConstruct
	public void init() {
			file1=null;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UploadedFile getFile1() {
		return file1;
	}

	public void setFile1(UploadedFile file1) {
		this.file1 = file1;
	}

	public boolean isDisplayAdd() {
		return displayAdd;
	}

	public void setDisplayAdd(boolean displayAdd) {
		this.displayAdd = displayAdd;
	}

	public String doUpdate(String fn,String ln,String em,String pss,String ss)throws IOException{
		 
		Learner u = new Learner();
		List<Learner> users =ser.listAllLearner();
		

		
		for (int i = 0; i < users.size(); i++) {
			u= users.get(i);
			System.out.println(u.getFirstName()+"11111111");
			System.out.println("");
			
			if (u.getIdUser()==2){
				System.out.println("22222222222");
				
				u.setFirstName(fn);
				u.setEmail(em);
				u.setLastName(ln);
				u.setPassword(pss);
				System.out.println("33333333333");
				
				ser.updateLearner(u);
				System.out.println(fn+"tsssaaaaaaab");
			}
		}
		return null;
	}


	private Blob ConverteBYTEtoBloob(byte[] contents) throws SerialException, SQLException {

		return new SerialBlob(contents);
	}

	public String doAdd() throws IOException, SerialException, SQLException{

		Learner u = new Learner();
		
		
		u.setFirstName(firstname);
		u.setLastName(lastname);
		u.setEmail(email);
		u.setPassword(password);
		u.setRole(Roles.Learner);
		u.setPicture(ConverteBYTEtoBloob(file1.getContents()));

		ser.addLearner(u);;

		return null;
	}

	public String dorenderAdd() {
		displayAdd = true;
		return null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public boolean isDisplayUpdate() {
		return displayUpdate;
	}

	public void setDisplayUpdate(boolean displayUpdate) {
		this.displayUpdate = displayUpdate;
	}



	public User getLogged() {
		return logged;
	}

	public void setLogged(Learner logged) {
		this.logged = logged;
	}

}
