package moocsWeb;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.UploadedFile;

import com.dropbox.core.DbxException;

import Extra.DropBox;
import moocs.ejb.bisness.ITrainerComiteeLocal;
import moocs.ejb.bisness.ITrainerLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.entity.Trainer;

@ManagedBean
@ViewScoped
public class TrainerComitee {
	@EJB
	ITrainerComiteeLocal TCL;
	@EJB
	IUserLocal IUL;
	@EJB
	ITrainerLocal ITL;

	private String urlCV;
	private boolean demande;
	private UploadedFile upf ;
	public ITrainerComiteeLocal getTCL() {
		return TCL;
	}
	public void setTCL(ITrainerComiteeLocal tCL) {
		TCL = tCL;
	}
	
	public UploadedFile getUpf() {
		return upf;
	}
	public void setUpf(UploadedFile upf) {
		this.upf = upf;
	}
	public String getUrlCV() {
		return urlCV;
	}
	public void setUrlCV(String urlCV) {
		this.urlCV = urlCV;
	}
	public boolean isDemande() {
		return demande;
	}
	
		
		public void setDemande(boolean demande) {
		this.demande = demande;
	}
		
		public void addCommittee() throws Exception{
		
			DropBox drop = new DropBox();
			String name =upf.getFileName();
			
			drop.setTest(0);
			drop.setIS(upf.getInputstream());
			drop.setTrainer("/Trainer/"+name);
			try {
				drop.UpLoad();
			} catch (DbxException e) {
			
				e.printStackTrace();
			}
			
			Trainer T=new Trainer();
			HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			Authentification auth=(Authentification) req.getSession().getAttribute("authentification");
			T.setIdUser(IUL.authenticate(auth.getEmail(),auth.getPassword()).getIdUser());
			T=ITL.affTrainer(T);
			T.setCvUrl("/Trainer/"+name);
			T.setDemande(true);		
			TCL.TrainerComitee(T, true, "/Trainer/"+name);
			
		}
}
