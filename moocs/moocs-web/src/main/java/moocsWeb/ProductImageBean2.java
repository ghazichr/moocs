package moocsWeb;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import moocs.ejb.bisness.ILearnerLocal;
import moocs.ejb.bisness.ITrainerLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.entity.Course;
import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Trainer;
import moocs.ejb.entity.User;



@ManagedBean
public class ProductImageBean2 {

	private StreamedContent image;


	//private int productId;
	

	private byte[] defaultPicture;

	@EJB
	private IUserLocal local ;
	
	
	User user = new User();
	
	


	@PostConstruct
	public void init()  {
		try {
		    
			HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			Authentification auth=(Authentification) req.getSession().getAttribute("authentification");
			User u = new User();
			
			u=local.authenticate(auth.getEmail(),auth.getPassword());

			Blob blob = user.getPicture();

			int blobLength;
			
				blobLength = (int) blob.length();
			  
			byte[] data = blob.getBytes(1, blobLength);
			image = new DefaultStreamedContent(new ByteArrayInputStream(data));
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		
		
		
		
	

	public StreamedContent getImage() {
		return image;
	}

	public void setImage(StreamedContent image) {
		this.image = image;
	}


	public byte[] getDefaultPicture() {
		return defaultPicture;
	}

	public void setDefaultPicture(byte[] defaultPicture) {
		this.defaultPicture = defaultPicture;
	}

	

}
