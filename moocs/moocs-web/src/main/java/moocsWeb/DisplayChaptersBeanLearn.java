package moocsWeb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import moocs.ejb.bisness.IChapterLocal;
import moocs.ejb.entity.Chapter;

@ManagedBean
@SessionScoped
public class DisplayChaptersBeanLearn {
	
	
	@EJB
	IChapterLocal ICL;
	
	
	private Chapter chapter;
	private List<Chapter> chapters;
	public Chapter getChapter() {
		return chapter;
	}
	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
	}
	public List<Chapter> getChapters() {
		return chapters;
	}
	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}
	
	@PostConstruct
	public void  init() {
		chapters=new ArrayList<Chapter>();
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		DisplayCoursesLearn dct=(DisplayCoursesLearn) req.getSession().getAttribute("displayCoursesLearn");
			
		chapters=ICL.afficherCourseChapter(dct.getCourse());
		chapter=new Chapter();
	}
	
	
	public String DoGoTOchapterDetail(){
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		DisplayCoursesLearn dct=(DisplayCoursesLearn) req.getSession().getAttribute("displayCoursesLearn");		
		chapters=ICL.afficherCourseChapter(dct.getCourse());
		
		
		return "/chapter/displayChaptersDetailsLearn?faces-redirect=true" ;
	}
	
	
	
	

}
