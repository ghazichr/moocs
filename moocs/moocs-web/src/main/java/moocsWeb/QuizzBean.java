package moocsWeb;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.primefaces.model.UploadedFile;

import moocs.ejb.bisness.IBadgeLearnerLocal;
import moocs.ejb.bisness.IQuestionLocal;
import moocs.ejb.bisness.IQuizzLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.bisness.IbadgeLocal;
import moocs.ejb.entity.Answer;
import moocs.ejb.entity.Badge;
import moocs.ejb.entity.Learner;
import moocs.ejb.entity.Qcm;
import moocs.ejb.entity.Question;
import moocs.ejb.entity.Quizz;

@ManagedBean
@SessionScoped
public class QuizzBean {
	@EJB
	IQuizzLocal serQuizz;
	@EJB
	IQuestionLocal serQuestion;
	@EJB
	IbadgeLocal ser;
	@EJB
	IBadgeLearnerLocal serBL;
	@EJB
	IUserLocal serUser;
	private List<Quizz> quizzs;
	private List<Question> questions;
	private List<Answer> answers;
	private List<Answer> chosen;
	private String nameBadge;
	private UploadedFile file1;
	private Quizz qui;
	private int quizzScore;
	private String name;
	private int note;
	private int qnumber;
	private boolean displayQuestion;
	private boolean displayall = true;
	private boolean displayUpdate;
	private boolean displayQuizz;
	private boolean displayScore;
	private List<String> mychoix = new ArrayList<String>();

	@PostConstruct
	public void init() {
		questions = new ArrayList<Question>();
		for (int i = 0; i < 20; i++) {
			mychoix.add("");
		}

	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public int getQnumber() {
		return qnumber;

	}

	public void setQnumber(int qnumber) {
		this.qnumber = qnumber;
	}

	public String doDelete() {
		serQuizz.remove(qui);
		return "/quizz/trainer/list?faces-redirect=true";
	}

	public String doUpdate() {
		serQuizz.update(qui);
		return "/quizz/trainer/list?faces-redirect=true";
	}

	public String doSelect() {
		return "/quizz/trainer/updateQuizz?faces-redirect=true";
	}
	public String doRenderTake()
	{
		return"/quizz/learner/takeQuizz?faces-redirect=true";
	}
	public String doTake() {
		quizzScore = 0;
		for (String g : mychoix) {
			if (g.equals("true")) {
				quizzScore++;
			}
		}
		if (quizzScore>=note)
		{
			HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			Authentification auth=(Authentification) req.getSession().getAttribute("authentification");
	
			serBL.addBadgeLearner(qui.getBadge(), (Learner)serUser.authenticate(auth.getEmail(),auth.getPassword()), quizzScore);
		}
		return "/quizz/trainer/list?faces-redirect=true";
	}

	public String doAdd() throws IOException, SerialException, SQLException {
		Quizz q = new Quizz();
		q.setName(name);
		q.setSuccessQuizz(note);
		q.setQuestions(questions);

		serQuizz.addQuizz(q);
		q = serQuizz.affQuizz(q);
		Qcm qcm = serQuizz.affQcm(q);
		for (Question quest : questions) {

			quest.setQcm(qcm);
			quest = serQuestion.affQuestion(quest);
		}
		q.setQuestions(questions);
		for (Question quest : questions) {
			answers = quest.getAnswers();
			for (Answer anser : answers) {
				anser.setQuestion(quest);
			}
			quest.setAnswers(answers);
		}
		serQuizz.update(q);
		Badge b1 = new Badge();
		b1.setNameBadge(nameBadge);
		b1.setImage(ConverteBYTEtoBloob(file1.getContents()));
		ser.addBadge(b1);
		b1 = ser.affBadge(b1);
		b1.setQuizz(q);
		ser.update(b1);
		return "/quizz/trainer/list?faces-redirect=true";


	}

	private Blob ConverteBYTEtoBloob(byte[] contents) throws SerialException, SQLException {

		return new SerialBlob(contents);
	}

	public String displayQuestion() {
		displayQuestion = true;

		for (int i = 0; i < qnumber; i++) {
			Question q = new Question();
			answers = new ArrayList<Answer>();
			Answer a1 = new Answer();
			Answer a2 = new Answer();
			Answer a3 = new Answer();
			Answer a4 = new Answer();
			answers.add(a1);
			answers.add(a2);
			answers.add(a3);
			answers.add(a4);
			q.setAnswers(answers);
			questions.add(q);

		}
		return null;
	}

	public boolean isDisplayQuestion() {
		return displayQuestion;
	}

	public void setDisplayQuestion(boolean displayQuestion) {
		this.displayQuestion = displayQuestion;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDisplayall() {
		return displayall;
	}

	public void setDisplayall(boolean displayall) {
		this.displayall = displayall;
	}

	public boolean isDisplayUpdate() {
		return displayUpdate;
	}

	public void setDisplayUpdate(boolean displayUpdate) {
		this.displayUpdate = displayUpdate;
	}

	public List<Quizz> getQuizzs() {
		quizzs = serQuizz.afficherAllQuizz();
		return quizzs;
	}

	public void setQuizzs(List<Quizz> quizzs) {
		this.quizzs = quizzs;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public Quizz getQui() {
		return qui;
	}

	public void setQui(Quizz qui) {
		this.qui = qui;
	}

	public boolean isDisplayQuizz() {
		return displayQuizz;
	}

	public void setDisplayQuizz(boolean displayQuizz) {
		this.displayQuizz = displayQuizz;
	}

	public List<Answer> getChosen() {
		return chosen;
	}

	public void setChosen(List<Answer> chosen) {
		this.chosen = chosen;
	}

	public int getQuizzScore() {
		return quizzScore;
	}

	public void setQuizzScore(int quizzScore) {
		this.quizzScore = quizzScore;
	}

	public boolean isDisplayScore() {
		return displayScore;
	}

	public void setDisplayScore(boolean displayScore) {
		this.displayScore = displayScore;
	}

	public List<String> getMychoix() {
		return mychoix;
	}

	public void setMychoix(List<String> mychoix) {
		this.mychoix = mychoix;
	}

	public UploadedFile getFile1() {
		return file1;
	}

	public void setFile1(UploadedFile file1) {
		this.file1 = file1;
	}

	public String getNameBadge() {
		return nameBadge;
	}

	public void setNameBadge(String nameBadge) {
		this.nameBadge = nameBadge;
	}

}
