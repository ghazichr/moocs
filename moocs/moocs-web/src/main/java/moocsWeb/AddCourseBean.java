package moocsWeb;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64.Encoder;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import javax.sql.rowset.serial.SerialException;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

import moocs.ejb.bisness.IChapterLocal;
import moocs.ejb.bisness.ICourseManagementLocal;
import moocs.ejb.entity.Chapter;
import moocs.ejb.entity.Course;
import moocs.ejb.entity.Trainer;

@ManagedBean
@ViewScoped
public class AddCourseBean {
	@EJB
	ICourseManagementLocal ICML;
	@EJB
	IChapterLocal ICL;
	
    private String titel;
    private String category;
    private String description;
    private String urlpdf;
    private String urlVideo;
    private String enddate;
    private String startdate;
    //private Date enddate1;    
    private Date date ; 
  //  private List<String> chapterdisplay;
    private int chaptersnum ;
    private int chapternum1;
    private boolean  display;
    private ArrayList<Chapter> chapters;
   // private UploadedFile uploadfile;
    private Part file;
  
    private String curlPDF1;
    private String curlPDF2;
    private String curlPDF3;
    

    private String curlVideo1;
    private String curlVideo2;
    private String curlVideo3;
    
  
    private String contained1;
    private String contained2;
    private String contained3;
    
    
    private String ctitel1;
    private String ctitel2;
    private String ctitel3;
    
    
    private String cdescription1;
    private String cdescription2;
    private String cdescription3;
    
    
    private ArrayList<Part>  parts1;
    private ArrayList<Part>  parts2;
    private Chapter Chapt;
    
    
 

	public ArrayList<Part> getParts1() {
		return parts1;
	}

	public void setParts1(ArrayList<Part> parts1) {
		this.parts1 = parts1;
	}

	public ArrayList<Part> getParts2() {
		return parts2;
	}

	public void setParts2(ArrayList<Part> parts2) {
		this.parts2 = parts2;
	}

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public String getCurlPDF1() {
		return curlPDF1;
	}

	public void setCurlPDF1(String curlPDF1) {
		this.curlPDF1 = curlPDF1;
	}

	public String getCurlPDF2() {
		return curlPDF2;
	}

	public void setCurlPDF2(String curlPDF2) {
		this.curlPDF2 = curlPDF2;
	}

	public String getCurlPDF3() {
		return curlPDF3;
	}

	public void setCurlPDF3(String curlPDF3) {
		this.curlPDF3 = curlPDF3;
	}

	public String getCurlVideo1() {
		return curlVideo1;
	}

	public void setCurlVideo1(String curlVideo1) {
		this.curlVideo1 = curlVideo1;
	}

	public String getCurlVideo2() {
		return curlVideo2;
	}

	public void setCurlVideo2(String curlVideo2) {
		this.curlVideo2 = curlVideo2;
	}

	public String getCurlVideo3() {
		return curlVideo3;
	}

	public void setCurlVideo3(String curlVideo3) {
		this.curlVideo3 = curlVideo3;
	}

	public String getContained1() {
		return contained1;
	}

	public void setContained1(String contained1) {
		this.contained1 = contained1;
	}

	public String getContained2() {
		return contained2;
	}

	public void setContained2(String contained2) {
		this.contained2 = contained2;
	}

	public String getContained3() {
		return contained3;
	}

	public void setContained3(String contained3) {
		this.contained3 = contained3;
	}

	public String getCtitel1() {
		return ctitel1;
	}

	public void setCtitel1(String ctitel1) {
		this.ctitel1 = ctitel1;
	}

	public String getCtitel2() {
		return ctitel2;
	}

	public void setCtitel2(String ctitel2) {
		this.ctitel2 = ctitel2;
	}

	public String getCtitel3() {
		return ctitel3;
	}

	public void setCtitel3(String ctitel3) {
		this.ctitel3 = ctitel3;
	}

	public String getCdescription1() {
		return cdescription1;
	}

	public void setCdescription1(String cdescription1) {
		this.cdescription1 = cdescription1;
	}

	public String getCdescription2() {
		return cdescription2;
	}

	public void setCdescription2(String cdescription2) {
		this.cdescription2 = cdescription2;
	}

	public String getCdescription3() {
		return cdescription3;
	}

	public void setCdescription3(String cdescription3) {
		this.cdescription3 = cdescription3;
	}

	public Chapter getChapt() {
		return Chapt;
	}

	public void setChapt(Chapter chapt) {
		Chapt = chapt;
	}



	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(ArrayList<Chapter> chapters) {
		this.chapters = chapters;
	}

	@PostConstruct
    public void init(){
    	setDisplay(false);
    	chapters=new ArrayList<Chapter>();   	
    	parts1=new ArrayList<Part>();
    	parts2=new ArrayList<Part>();
    }
    
	public int getChapternum1() {
		return chapternum1;
	}



	public void setChapternum1(int chapternum1) {
		this.chapternum1 = chapternum1;
	}



	public boolean isDisplay() {
		return display;
	}
	public void setDisplay(boolean display) {
		this.display = display;
	}
	//public List<String> getChapterdisplay() {
	//	return chapterdisplay;
	//}
	//public void setChapterdisplay(List<String> chapterdisplay) {
	//	this.chapterdisplay = chapterdisplay;
	//}
	public int getChaptersnum() {
		return chaptersnum;
	}
	public void setChaptersnum(int chaptersnum) {
		this.chaptersnum = chaptersnum;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTitel() {
		return titel;
	}
	public void setTitel(String titel) {
		this.titel = titel;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrlpdf() {
		return urlpdf;
	}
	public void setUrlpdf(String urlpdf) {
		this.urlpdf = urlpdf;
	}
	public String getUrlVideo() {
		return urlVideo;
	}
	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
    public String addCourse() throws IOException, SerialException, SQLException{
    	Course c=new Course();
    	c.setCategory(category);
    	c.setDescription(description);
    	c.setUrlPdf(urlpdf);
    	c.setUrlVideo(urlVideo);
    	c.setTitle(titel);
    	ICML.addCourse(c);

    	Course cc=new Course();
    	 cc=ICML.findCourse(c);
       //for (int i = 0; i < chapters.size(); i++) {
		
    	   
		//parts2.get(i);
		// String pt1 = new Scanner(parts1.get(i).getInputStream())
        //         .useDelimiter("\\A").next();                 		 
        // Blob b1 = new javax.sql.rowset.serial.SerialBlob(pt1.getBytes());  
         
        // String pt2 = new Scanner(parts2.get(i).getInputStream())
        //         .useDelimiter("\\A").next();                        
        // Blob b2 = new javax.sql.rowset.serial.SerialBlob(pt1.getBytes());  
	
		//chapters.get(i).setPic1(b1);
		//chapters.get(i).setPic2(b2);
		
	    //}
    	
    	for (Chapter chapts : chapters) {
				
			chapts.setCourse(cc);
			ICL.addChapter(chapts);	
		}

    	return null;
    }
    public String displayChapter(){
    	display=true;
    	chapternum1=chaptersnum;
    	

    	for(int i=0;i<chaptersnum;i++){
    		Part p=null;
    		parts1.add(p);
    		parts2.add(p);
    		Chapter c=new Chapter();
    		c.setContained("");
    		chapters.add(c);

    	}
    	return null;
    }
    public String addchapters(){

    	
    	return null;
    }
    public void save() {
        try (InputStream input =  file.getInputStream()) {
        	//Files.copy(input, new File(, file.getName()).toPath());
        	 
        }
        catch (IOException e) {
            // Show faces message?
        }
    }
}
