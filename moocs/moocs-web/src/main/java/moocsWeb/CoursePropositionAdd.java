package moocsWeb;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import moocs.ejb.bisness.ICourseManagementLocal;
import moocs.ejb.entity.Course;
import moocs.ejb.entity.Trainer;

@ManagedBean
@ViewScoped
public class CoursePropositionAdd  {
	@EJB
ICourseManagementLocal icml;
	private String Title;
	private String Category;
	private String description;
	private int idTrainer;
	public ICourseManagementLocal getIcml() {
		return icml;
	}
	public void setIcml(ICourseManagementLocal icml) {
		this.icml = icml;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	 
	public int getIdTrainer() {
		return idTrainer;
	}
	public void setIdTrainer(int idTrainer) {
		this.idTrainer = idTrainer;
	}
	public String addCourseProp(){
		Course c= new Course();
		c.setCategory(Category);
		c.setDescription(description);
		c.setTitle(Title);
		//Trainer T =new Trainer();
		//c.setTrainer(T);
		
		icml.addCourse(c);
		return null;
	}

}
