package moocsWeb;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;


import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import moocs.ejb.bisness.ILearnerLocal;

import moocs.ejb.entity.Learner;



@ManagedBean
public class ProductImageBean {

	private StreamedContent image;


	//private int productId;
	

	private byte[] defaultPicture;

	@EJB
	private ILearnerLocal local;
	
	Learner learner = new Learner();


	@PostConstruct
	public void init()  {
		try {
		    learner.setIdUser(138);
			learner=local.affLearner(learner);

			Blob blob = learner.getPicture();

			int blobLength;
			
				blobLength = (int) blob.length();
			  
			byte[] data = blob.getBytes(1, blobLength);
			image = new DefaultStreamedContent(new ByteArrayInputStream(data));
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		
		
		
		
	

	public StreamedContent getImage() {
		return image;
	}

	public void setImage(StreamedContent image) {
		this.image = image;
	}


	public byte[] getDefaultPicture() {
		return defaultPicture;
	}

	public void setDefaultPicture(byte[] defaultPicture) {
		this.defaultPicture = defaultPicture;
	}

	

}
