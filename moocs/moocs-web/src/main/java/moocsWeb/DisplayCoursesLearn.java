package moocsWeb;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import moocs.ejb.bisness.ICourseManagementLocal;
import moocs.ejb.bisness.IUserLocal;
import moocs.ejb.entity.Course;

@ManagedBean
@SessionScoped
public class DisplayCoursesLearn {
	
	@EJB
	ICourseManagementLocal ICML;
	@EJB
	IUserLocal IUL;
	
	private List<Course> courses ;
	private Course course;
	
	

	public List<Course> getCourses() {
		return courses;
	}
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}





	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	@PostConstruct
	public void init() {
		course=new Course();
		courses=new ArrayList<Course>();
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		TrainerBean auth=(TrainerBean) req.getSession().getAttribute("trainerBean");
		courses=ICML.findallCourseByUser(auth.getSelectedTrainer().getIdUser());			
}
	
	
	public String  DogoTOchapters() {
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		TrainerBean auth=(TrainerBean) req.getSession().getAttribute("trainerBean");
		courses=ICML.findallCourseByUser(auth.getSelectedTrainer().getIdUser());	
		return "/chapter/displayChaptersLearn?faces-redirect=true";
		
	}
	
	
	
	
}
